package main

import(
	"fmt"
	"net/smtp"
)

type Report struct {
	id string
	err error
	errs map[string]error
	cfg Config
	switched bool
}

func NewReport(cfg Config) *Report {
	r := &Report{}
	r.cfg = cfg
	r.errs = make(map[string]error)
	return r
}

func (r *Report) String() string {
	s := "Deployment Report\n"
	s += "-----------------\n\n"
	s += fmt.Sprintf("%s\n\n", r.Title())
	if r.err != nil {
		s += fmt.Sprintf("%s\n\n", r.err.Error())
	}
	for name, _ := range r.cfg {
		result := "OK"
		if err,ok := r.errs[name]; ok {
			result = err.Error()
		}
		s += fmt.Sprintf("%s: %s\n\n", name, result)
	}
	return s
}

func (r *Report) Title() string {
	if r.Success() {
		return "Deployment Success"
	}
	return "Deployment failure"
}

func (r *Report) Success() bool {
	if r.err != nil {
		return false
	}
	for name, _ := range r.cfg {
		if err,ok := r.errs[name]; ok && err != nil {
			return false
		}
	}
	return true
}

// Email's report using global flag details
func (r *Report) Send() (err error) {
	// Set up authentication information.
	auth := smtp.PlainAuth("", notifyUser, notifyPass, notifyServer)
	to := []string{notifyTo}
	host := fmt.Sprintf("%s:%d", notifyServer, notifyPort)
	msg := ""
	msg += fmt.Sprintf("To: <%s>\n", notifyTo)
	msg += fmt.Sprintf("From: <%s>\n", notifyFrom)
	msg += fmt.Sprintf("Subject: %s\n", r.Title())
	msg += fmt.Sprintf("\n\n%s", r.String())
	err = smtp.SendMail(host, auth, notifyFrom, to, []byte(msg))
	if err != nil {
		return
	}
	return
}

// Convert an error to a report
func reportError(err error) *Report {
	r := NewReport(nil)
	r.err = err
	return r
}

