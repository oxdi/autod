package main

import(
	"encoding/json"
	"fmt"
	"reflect"
	"os"
	"io/ioutil"
	"strings"
	"path"
)

type pseudoConfig struct{
	path string
}

func (c pseudoConfig) MarshalNix() (b []byte, err error) {
	if c.path == "" {
		return []byte("{}"), nil
	}
	return []byte(fmt.Sprintf(`let
		cfg = { imports = [ %s ]; } // config.services.autodeploy.extraConfig;
	in {...}: cfg`, c.path)), nil
}

type ContainerConfig struct {
	Enable bool              `nix:"enable"`
	PrivateNetwork bool      `nix:"privateNetwork"`
	MacvlanInterface string  `nix:"macvlanInterface"`
	MacvlanAddress string    `nix:"macvlanAddress"`
	MacvlanPrefixLength int  `nix:"macvlanPrefixLength"`
	LinkJournal string `nix:"linkJournal"`
	GrantCapabilities []string `nix:"grantCapabilities"`
	Config pseudoConfig `nix:"config"` // this is not parsed, but will generate content when MarshalNix called
}

type DeploymentConfig struct {
	Url string `nix:"url"`
	Branch string `nix:"branch"`
	Key string `nix:"key"`
	Notify []string `nix:"notify"`
	Backup string `nix:"backup"`
	ContainerConfig *ContainerConfig `nix:"containerConfig"`
}

// Checkout the repo and write the container config to the state dir
func (c *DeploymentConfig) WriteConfig(name, root string) (cfgpath string, err error) {
	// checkout repo to root/repo
	repo, err := fetch(c.Url, c.Branch, c.Key, root)
	if err != nil {
		return
	}
	// set the container's config
	c.ContainerConfig.Config.path = path.Join(repo, ".nix/configuration.nix")
	if _, err = os.Stat(c.ContainerConfig.Config.path); os.IsNotExist(err) {
		err = fmt.Errorf("Repository %s does not have a .nix/configuration.nix file", c.Url)
		return
	}
	// Generate config to root/container.nix
	b,err := MarshalNix(c.ContainerConfig)
	if err != nil {
		return
	}
	// convert to nixos module
	b = []byte(fmt.Sprintf("{config, pkgs, ...}: { containers.%s = %s; }", name, string(b)))
	cfgpath = path.Join(root, "container.nix")
	err = ioutil.WriteFile( cfgpath, b, 0640 )
	if err != nil {
		return
	}
	return
}

type Config map[string]*DeploymentConfig

// Write config to outputPath
// Returns a hash for the build
func (c Config) WriteConfig() (r *Report) {
	containers := make([]string, 0)
	r = NewReport(c)
	for name,cfg := range c {
		root := path.Join(stateDir, "containers", name)
		path, err := cfg.WriteConfig(name, root)
		if err != nil {
			r.errs[name] = err
		}
		containers = append(containers, path)
	}
	m := fmt.Sprintf("{config, pkgs, ...}: { imports = [%s]; }", strings.Join(containers, " "))
	err := ioutil.WriteFile(outputPath, []byte(m), 0640)
	if err != nil {
		r.err = err
	}
	return
}

type Marshaler interface {
	MarshalNix() ([]byte, error)
}

func MarshalNix(v interface{}) (b []byte, err error) {
	// Allow types to override default behaviour via Marshaler interface
	m, ok := v.(Marshaler)
	if ok {
		return m.MarshalNix()
	}
	// Perform marshal via reflection
	vv := reflect.ValueOf(v)
	if vv.Kind() == reflect.Ptr {
		vv = vv.Elem()
	}
	switch vv.Kind() {
	case reflect.Slice:
		b = make([]byte, 1)
		b[0] = '['
		for i := 0; i < vv.Len(); i++ {
			bv,err := MarshalNix(vv.Index(i).Interface())
			if err != nil {
				return nil, err
			}
			b = append(b, bv...)
			b = append(b, ' ')
		}
		b = append(b, ']')
	case reflect.Struct:
		t := vv.Type()
		b = make([]byte, 1)
		b[0] = '{'
		for i := 0; i < vv.NumField(); i++ {
			bv, err := MarshalNix(vv.Field(i).Interface())
			if err != nil {
				return nil, err
			}
			f := t.Field(i)
			name := f.Tag.Get("nix")
			if name == "" {
				name = f.Name
			}
			b = append(b, []byte(fmt.Sprintf("%s = %s; ", name, string(bv)))...)
		}
		b = append(b, '}')
	case reflect.Map:
		b = make([]byte, 1)
		b[0] = '{'
		keys := vv.MapKeys()
		for _, k := range keys {
			if k.Kind() != reflect.String {
				return nil, fmt.Errorf("it's not possible to marshal maps with non-string keys to nix")
			}
			bv, err := MarshalNix(vv.MapIndex(k).Interface())
			if err != nil {
				return nil, err
			}
			b = append(b, []byte(fmt.Sprintf("%s = %s; ", k.String(), string(bv)))...)
		}
		b = append(b, '}')
	case reflect.String:
		b = []byte(fmt.Sprintf(`''%s''`, v))
	case reflect.Int, reflect.Bool:
		b, err = json.Marshal(v)
	default:
		err = fmt.Errorf("cannot marshal %v to nix", v)
	}
	return
}

func ParseConfig(b []byte) (c Config, err error) {
	err = json.Unmarshal(b, &c)
	if err != nil {
		return
	}
	return
}
