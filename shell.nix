let
    pkgs = import <nixpkgs> {};
    stdenv = pkgs.stdenv;
in 
{
    goEnv = stdenv.mkDerivation rec {
        name = "development";
        version = "nightly";
        src = ./.;
        buildInputs = [ 
            pkgs.go 
        ];
        shellHook = ''
            export GOROOT=${pkgs.go}/share/go
            export GOPATH=$(pwd)
            source $GOROOT/misc/bash/go
        '';
    };
}
