package main

import (
	"testing"
	"bytes"
	"strings"
)

// A dummy deployment key for the example repo to test that 
// GIT_SSH is doing it's job as expected
const test_key = `
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAn5kECUDiZdo8QXKRaUszg27k7jPuOLRrIPbHLLwSzLSxoRBb
FW94cC1srwdQo+QWpvsBzUOB9BaY0JekJxEJ7LbVp4NC4iUXHcpSqUa58PZhg6v9
y/qxAsBxuNL5EENzXY71V8STg7IlBs2ucU0MGkxfE94KSFc18pRJnvcKc+6qDrgZ
LFIVkwaIU/vPGBmNahmUZz+2QUWkJ8Jp7bMtQguoLUKWPsd3WpF/0bHzQF/vtfWx
r4ewdKgzyxQZbhU3E1RqbiGI8SYMQISzNbyHxePinTNs11UvpMKd0OMyGkW8W1M2
Tap4VH0QJYvBHIwWzQxsba1i7swj7XB9Oi/2+wIDAQABAoIBACXSUlWLDdwLUtpD
t1gXnTaZkiEtJ1hC0Vx3fw4H6AChFbNQt/UlfIpBCY+VETQ1unSJ6YxmYiahMEfG
l/02GX8CeTGAdto25fqGVv6Mp0YGbWeR28x6NXaseobNYH7NLUrPtS6+hCLilcm9
9ogdNTAKR0HjDdbE7CD3qVbFvrFoTvYdHxV0sLvk+4TBlSVErol6nis/X25uvXht
3hAsL2mxxT9LgsPFv79rEAah3mVjYM8H3rd1K5b/hM1RuSdxy6X+dBhuc6NUwxpb
gmwd8MSSO4nsq0NirHtBJ5Ff8RWFwltR3+SufNYK594jj+nL/0Y7tuwAyilZWLlj
cvrSrnECgYEA08/pCM/AHQfRke1hZE8dZ3AYnruVbudnzBmwGo/m6wh3rJdpUmbn
MLR3nS4zPyCUKh6WCQJsSedaqM1LU5arEQ4G92Snx087owSBUifj5ReWGCH3ijW/
lAjHiPvLfKN5DBA77jwF3un0RgDZB/+2omC3JADjbCjvhqetKXaLfakCgYEAwOSH
eWpvrsnY+soksi0XDVTjrMSLuWa4em2FG21jCXmxzUAGXKmIj3w0PAxhCJAukZSH
AiQzIrRZh4yNgg9XxKy0L/xDeea+JYk+Qrb5Vwf9HtspmjQJ0Pujee5vcO+XPFl1
3/f0tySd9JpuJvvFIqFCwNbQOY91fpXRoOakTgMCgYEArLs/mjz5uXL3195S7x+J
piIHIeNt4Kx7vDkyoOa7dO5J0gkEDfv3C4QQi7Dyf8eMrLCWwDAmqTovd7s7c6ZK
STU8PyHppSMbSjzRTXNUaS4L7BA/M4nTeuCSAEb+0dzqSQkwJn7ydrAp0bC9TMWf
ZdojYAwNatYTR6eUEl0nMkkCgYEAvw/eDOrbkwNGVCt0K7Sz97WVCuVoDQ3tPv9K
cD9qcNFEx6xaYOBgbte1GaGdX5EJP5QiIoHe0MpZQAV9iU1PP3VdyYYTBCUFRULr
E8QPuCAmnbCyRZaz/neylq0RX4lCgTmPo3gEwn+XodhxsVWDD+kxqLdeaxVpaShX
CwxJxScCgYEAn3sO/PO27Ws6aYjn+SxHwML3g8OWOGpRmc7vX9cH1zbydahMxlKt
/lAD0yr9yLYdbUXhDslqMPvVDsgGL8p/wIgZFLQbkChsGhnIqpVufQASYl6Qu5cx
YJV6vfZqsMpe894mAiDbKqfOL7MxgHEPn9kc/tOmeE3oyFNG1Tp+VhU=
-----END RSA PRIVATE KEY-----
`
var json_test_key = strings.Replace(test_key, "\n", "\\n", -1)

var tcjson = []byte(`{"app1":{"branch":"production","containerConfig":{"enable":true,"grantCapabilities":["CAP_MKNOD"],"linkJournal":"guest","macvlanAddress":"10.0.1.30","macvlanInterface":"enp5s0","macvlanPrefixLength":24,"privateNetwork":true},"key":"`+json_test_key+`","notify":["chris@example.com"],"url":"git@bitbucket.org:oxdi/example.git"}}`)
var tcnix = []byte(`{app1 = {url = ''git@bitbucket.org:oxdi/example.git''; branch = ''production''; key = ''`+test_key+`''; notify = [''chris@example.com'' ]; backup = ''''; containerConfig = {enable = true; privateNetwork = true; macvlanInterface = ''enp5s0''; macvlanAddress = ''10.0.1.30''; macvlanPrefixLength = 24; linkJournal = ''guest''; grantCapabilities = [''CAP_MKNOD'' ]; config = {}; }; }; }`)


func TestParse(t *testing.T) {
	cfgs, err := ParseConfig(tcjson)
	if err != nil {
		t.Fatal(err)
	}
	cfg, exists := cfgs["app1"]
	if !exists {
		t.Fatalf("Expected to have key 'app1'")
	}
	if cfg.Branch != "production" {
		t.Fatalf("Expected cfgCase0.app1.branch to be 'production'")
	}
	if cfg.ContainerConfig == nil {
		t.Fatalf("missing ContainerConfig")
	}
	if !cfg.ContainerConfig.Enable {
		t.Fatalf("expected app1 to be enabled")
	}
	if len(cfg.ContainerConfig.GrantCapabilities) != 1 {
		t.Fatalf("expected 1 capability")
	}
	if cfg.ContainerConfig.GrantCapabilities[0] != "CAP_MKNOD" {
		t.Fatalf("expected CAP_MKNOD capability")
	}
	if cfg.ContainerConfig.LinkJournal != "guest" {
		t.Fatalf("expected LinkJournal to be 'guest'")
	}
	if cfg.ContainerConfig.MacvlanAddress != "10.0.1.30" {
		t.Fatalf("expected MacvlanAddress to be '10.0.1.30'")
	}
	if cfg.ContainerConfig.MacvlanInterface != "enp5s0" {
		t.Fatalf("expected MacvlanInterface to be 'enp5s0'")
	}
	if cfg.ContainerConfig.MacvlanPrefixLength != 24 {
		t.Fatalf("expected MacvlanPrefixLength to be 24")
	}
	if !cfg.ContainerConfig.PrivateNetwork {
		t.Fatalf("expected privateNetwork to be true")
	}
	if cfg.Key != test_key {
		t.Fatalf("expected key to be '%s'", test_key)
	}
	if len(cfg.Notify) != 1 {
		t.Fatalf("expected 1 email address in notify list")
	}
	if cfg.Notify[0] != "chris@example.com" {
		t.Fatalf("expected chris@example.com in notify list")
	}
	if cfg.Url != "git@bitbucket.org:oxdi/example.git" {
		t.Fatalf("git url was not as expected")
	}
}

func TestToNixAndBack(t *testing.T) {
	cfgs, err := ParseConfig(tcjson)
	if err != nil {
		t.Fatal(err)
	}
	b, err := MarshalNix(cfgs)
	if err != nil {
		t.Fatal(err)
	}
	if len(b) == 0 {
		t.Fatalf("expected to get marshaled nix bytes")
	}
	if !bytes.Equal(b, tcnix) {
		t.Fatalf("expected output to match tcnix.\nexp: %s\ngot: %s\n", string(tcnix), string(b))
	}
}
