package main

import(
	"flag"
	"io/ioutil"
	"log"
	"path"
	"bytes"
	"os"
)

var stateDir string
var configPath string
var outputPath string
var notifyTo string
var notifyFrom string
var notifyServer string
var notifyUser string
var notifyPass string
var notifyPort int

func init() {
	flag.StringVar(&stateDir, "s", "/var/autod", "State dir where repository checkouts take place")
	flag.StringVar(&configPath, "c", "/etc/autod.conf", "Path to configuration file")
	flag.StringVar(&outputPath, "o", "/etc/nixos/containers.nix", "Output nixos module path")

	flag.StringVar(&notifyTo, "to", "", "Email address to notify of build success/failure")
	flag.StringVar(&notifyFrom, "from", "", "Email address to notify of build success/failure")
	flag.StringVar(&notifyServer, "host", "", "SMTP server to use to send notification")
	flag.IntVar(&notifyPort, "port", 25, "SMTP server to use to send notification")
	flag.StringVar(&notifyUser, "user", "", "SMTP username")
	flag.StringVar(&notifyPass, "pass", "", "SMTP password")

	flag.Parse()
}

// Reads the config json and outputs the nix file to global outputPath
func GenerateConfig() (r *Report) {
	// parse config
	b,err := ioutil.ReadFile(configPath)
	if err != nil {
		return reportError(err)
	}
	cfg,err := ParseConfig(b)
	if err != nil {
		return reportError(err)
	}
	r = cfg.WriteConfig()
	if !r.Success() {
		return
	}
	return
}

// Same as GenerateConfig but also runs nixos-rebuild if required.
func GenerateAndRebuild() (r *Report) {
	var err error
	// get last build hash
	hashPath := path.Join(stateDir, "build.sha1")
	var prevBuild []byte
	var thisBuild []byte
	if _, err = os.Stat(hashPath); err == nil {
		prevBuild,err = ioutil.ReadFile(hashPath)
		if err != nil {
			return reportError(err)
		}
	}
	r = GenerateConfig()
	// generate new build hash
	if _, err = os.Stat(hashPath); err == nil {
		thisBuild,err = hash( path.Join(stateDir, "containers") )
		if err != nil {
			r.err = err
			return
		}
	}
	if prevBuild == nil || !bytes.Equal(prevBuild,thisBuild) {
		r.switched = true
		err = rebuild("switch")
		if err != nil {
			r.err = err
		}
		err = ioutil.WriteFile(hashPath, thisBuild, 0640)
		if err != nil {
			r.err = err
			return
		}
	}
	return
}

func main() {
	report := GenerateAndRebuild()
	if report.switched {
		report.Send()
	}
	if !report.Success() {
		log.Fatal(report.String())
	}
}



