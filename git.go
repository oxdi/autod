package main

import(
	"os/exec"
	"os"
	"io/ioutil"
	"path"
	"strings"
	"fmt"
)

// Clones repository to dest (if missing), checks out 
// specified branch and pulls recent changes
func fetch(url, branch, key, dest string) (repo string, err error) {
	// clone
	if _, err = os.Stat(dest); os.IsNotExist(err) {
		err = os.MkdirAll(dest, 0770)
		if err != nil {
			return
		}
	}
	if _, err = os.Stat(path.Join(dest, "repo")); os.IsNotExist(err) {
		err = gitWithKey(dest, key, "clone", url, "repo")
		if err != nil {
			return
		}
	}
	repo = path.Join(dest, "repo")
	// fetch all branches
	err = gitWithKey(repo, key, "pull", "--all")
	if err != nil {
		return
	}
	// fetch all branches
	err = gitWithKey(repo, key, "checkout", branch)
	if err != nil {
		return
	}
	// pull latest
	err = gitWithKey(repo, key, "pull", "origin", branch)
	if err != nil {
		return
	}
	return
}

func git(cwd string, args ...string) (err error) {
	return gitWithKey(cwd, "", args...)
}

// Clone url to dest
// If key is set then ensure git uses that key for SSH auth
func gitWithKey(cwd string, key string, args ...string) (err error) {
	path, err := exec.LookPath("git")
	if err != nil {
		return fmt.Errorf("Git executable not found in PATH")
	}
	sshPath, err := exec.LookPath("ssh")
	if err != nil {
		return fmt.Errorf("SSH executable not found in PATH")
	}
	cmd := exec.Command(path, args...)
	// if key set then write the key to a temp location and set GIT_SSH
	if key != "" {
		// writeout the keyfile
		keyfile,err := ioutil.TempFile("", "key")
		if err != nil {
			return err
		}
		defer func(){
			keyfile.Close()
			os.Remove(keyfile.Name())
		}()
		_, err = keyfile.WriteString(key)
		if err != nil {
			return fmt.Errorf("Could not write out deployment keyfile: %s", err.Error())
		}
		keyfile.Close()
		// make a git wrapper
		wrapper,err := ioutil.TempFile("", "git")
		if err != nil {
			return err
		}
		defer func(){
			wrapper.Close()
			os.Remove(wrapper.Name())
		}()
		_, err = wrapper.WriteString(fmt.Sprintf("#!/bin/sh\n%s -i \"%s\" \"$@\"", sshPath, keyfile.Name()))
		if err != nil {
			return fmt.Errorf("Could not write git wrapper: %s", err.Error())
		}
		err = wrapper.Chmod(0700)
		if err != nil {
			return fmt.Errorf("Could not make git wrapper executable: %s", err.Error())
		}
		wrapper.Close()
		cmd.Env = append(os.Environ(), fmt.Sprintf("GIT_SSH=%s", wrapper.Name()))
	}
	if cwd != "" {
		cmd.Dir = cwd
	}
	out, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("Error while executing: %s %s\n%s\n%s", path, strings.Join(args, " "), out, err.Error())
	}
	return
}
