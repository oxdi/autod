package main

import(
	"testing"
	"io/ioutil"
	"os"
	"path"
	"fmt"
)

func TestMain(t *testing.T) {
	var err error
	// Use a tmp stateDir
	stateDir, err = ioutil.TempDir("", "autodtest")
	if err != nil {
		t.Fatal(err)
	}
	defer func(){
		err = os.RemoveAll(stateDir)
		if err != nil {
			t.Fatal(err)
		}
	}()
	err = os.Chmod(stateDir, 0777)
	if err != nil {
		t.Fatal(err)
	}
	// Use a tmp outputPath
	outputPath = path.Join( stateDir, "configuration.nix" )
	// Write a tmp config
	configPath = path.Join(stateDir, "autod.conf")
	err = ioutil.WriteFile(configPath, tcjson, 0640)
	if err != nil {
		t.Fatal(err)
	}
	// Run it
	r := GenerateConfig()
	if !r.Success() {
		t.Fatal(r.String())
	}
	// Read back the output
	b, err := ioutil.ReadFile(outputPath)
	if err != nil {
		t.Fatal(err)
	}
	exp := fmt.Sprintf(`{config, pkgs, ...}: { imports = [%s]; }`, path.Join(stateDir, "containers", "app1", "container.nix"))
	if string(b) != exp {
		t.Fatalf("unexpected configuration output.\nexp: %s\ngot: %s", exp, string(b))
	}
	h,err := hash(path.Join(stateDir, "containers"))
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println("hash", h)
	//notifyServer = "smtp.mandrillapp.com"
	//notifyUser = "chris@oxdi.eu"
	//notifyPass = "4VZedDkQVES7G_dMRUycpg"
	//notifyTo = "chris@oxdi.eu"
	//notifyFrom = "web@cycleapp.com"
	//err = r.Send()
	//if err != nil {
	//	t.Fatal(err)
	//}
}

