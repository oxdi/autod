
package main

import(
	"os/exec"
	"fmt"
	"strings"
	"path/filepath"
	"crypto/sha1"
)

// Run nixos-rebuild command
func rebuild(args ...string) (err error) {
	path, err := exec.LookPath("nixos-rebuild")
	if err != nil {
		return fmt.Errorf("nixos-rebuild executable not found in PATH")
	}
	cmd := exec.Command(path, args...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("Error while executing: %s %s\n%s", path, strings.Join(args, " "), out)
	}
	return
}

// Create a hash of all projects. 
// This is used to see if a rebuild is required
func hash(path string) (h []byte, err error) {
	gitPath, err := exec.LookPath("git")
	if err != nil {
		return nil, fmt.Errorf("git executable not found in PATH")
	}
	hasher := sha1.New()
	paths,err := filepath.Glob(fmt.Sprintf("%s/*/repo", path))
	for _, p := range paths {
		cmd := exec.Command(gitPath, "log", "-n1", "-q", "--oneline")
		cmd.Dir = p
		out,err := cmd.CombinedOutput()
		if err != nil {
			return nil, fmt.Errorf("Error while executing: %s\n%s", gitPath, out)
		}
		hasher.Write([]byte(out))
	}
	return hasher.Sum(nil), nil
}
